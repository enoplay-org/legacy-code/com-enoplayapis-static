package usecase

import (
	"fmt"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

type UserInteractor struct {
	UserRepository  domain.UserRepository
	ImageRepository domain.ImageRepository
}

func NewUserInteractor() *UserInteractor {
	return &UserInteractor{}
}

// GetByUsername gets a User by username
func (interactor *UserInteractor) GetByUsername(username string) (domain.User, error) {
	return interactor.UserRepository.FindByUsername(username)
}

func (interactor *UserInteractor) UpdateIcon(filePath string, user domain.User) (domain.User, error) {
	image := domain.Image{}
	image.Owner.UID = user.UID
	image.Owner.Type = domain.ImageOwnerUserIcon

	// Create New Image
	image, err := interactor.ImageRepository.Create(image, filePath)
	if err != nil {
		return domain.User{}, fmt.Errorf("Error creating new image - \n\t%v", err.Error())
	}

	// Delete Old Image
	interactor.ImageRepository.Delete(user.Media.Icon.UID)

	// Update Image
	user.Media.Icon.UID = image.UID
	user.Media.Icon.Source = image.Source

	user, err = interactor.UserRepository.UpdateIcon(user)
	if err != nil {
		interactor.ImageRepository.Delete(image.UID)
		return domain.User{}, fmt.Errorf("Error updating image - \n\t%v", err.Error())
	}

	return user, nil
}

func (interactor *UserInteractor) UpdateBanner(filePath string, user domain.User) (domain.User, error) {
	image := domain.Image{}
	image.Owner.UID = user.UID
	image.Owner.Type = domain.ImageOwnerUserBanner

	// Create New Image
	image, err := interactor.ImageRepository.Create(image, filePath)
	if err != nil {
		return domain.User{}, fmt.Errorf("Error creating new image - \n\t%v", err.Error())
	}

	// Delete Old Image
	interactor.ImageRepository.Delete(user.Media.Banner.UID)

	// Update Image
	user.Media.Banner.UID = image.UID
	user.Media.Banner.Source = image.Source

	user, err = interactor.UserRepository.UpdateBanner(user)
	if err != nil {
		interactor.ImageRepository.Delete(image.UID)
		return domain.User{}, fmt.Errorf("Error updating image - \n\t%v", err.Error())
	}

	return user, nil
}
