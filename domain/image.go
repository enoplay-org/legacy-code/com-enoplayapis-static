package domain

type ImageRepository interface {
	Get(uid string) (Image, error)
	Create(image Image, imageFilePath string) (Image, error)
	Delete(uid string) error
}

type Image struct {
	UID   string `json:"uid" bson:"uid"`
	Owner struct {
		UID  string `json:"uid" bson:"uid"`
		Type string `json:"type" bson:"type"` // user, game
	} `json:"owner" bson:"owner"`
	Source     string      `json:"source" bson:"source"`
	UploadData interface{} `json:"uploadData,omitempty" bson:"uploadData,omitempty"`
}

type ImageDetails struct {
	Source string `json:"source" bson:"source"`
}

const (
	ImageOwnerUserIcon      = "user_icon"
	ImageOwnerUserBanner    = "user_banner"
	ImageOwnerGameThumbnail = "game_thumbnail"
	ImageOwnerGameImage     = "game_image"
)
