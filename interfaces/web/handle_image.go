package web

import (
	"fmt"
	"net/http"

	"github.com/go-zoo/bone"
	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

type ImageResultsRequestV0 struct {
	ID      string `json:"id"`
	Success bool   `json:"success"`
	URL     string `json:"kraked_url"`
}

type DeleteImageResponseV0 struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success,omitempty"`
}

type ImageInteractor interface {
	Get(uid string) (domain.Image, error)
	Delete(uid string) error
}

func (handler *WebHandler) ImageResults(res http.ResponseWriter, req *http.Request) {
	// Handle image processing response from 3rd party
}

func (handler *WebHandler) DeleteImage(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	imageUID := bone.GetValue(req, "uid")
	mediaImage, err := handler.ImageInteractor.Get(imageUID)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error finding image: %v", err.Error()))
		return
	}

	var imageDeleteErr error
	var isDeleted bool

	// User Image
	if mediaImage.Owner.Type == domain.ImageOwnerUserIcon ||
		mediaImage.Owner.Type == domain.ImageOwnerUserBanner {
		if mediaImage.Owner.UID == userUID.(string) {
			imageDeleteErr = handler.ImageInteractor.Delete(imageUID)
			isDeleted = true
		} else {
			imageDeleteErr = fmt.Errorf("Unauthorized access")
		}
	}

	// Game Image
	if (mediaImage.Owner.Type == domain.ImageOwnerGameThumbnail ||
		mediaImage.Owner.Type == domain.ImageOwnerGameImage) &&
		imageDeleteErr == nil {
		game, err := handler.GameInteractor.Get(mediaImage.Owner.UID)
		if err != nil {
			handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error finding game image owner: %v", err.Error()))
			return
		}

		if game.Publisher.UID == userUID.(string) {
			imageDeleteErr = handler.ImageInteractor.Delete(imageUID)
			isDeleted = true
		} else {
			imageDeleteErr = fmt.Errorf("Unauthorized access")
		}
	}

	// Ensure no delete errors
	if imageDeleteErr != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error deleting image: %v", imageDeleteErr))
		return
	}

	// Ensure User or Game Image was deleted
	if isDeleted == false {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Error deleting image of type: %v", mediaImage.Owner.Type))
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, DeleteImageResponseV0{
		Message: "Image deleted!",
		Success: true,
	})
}
