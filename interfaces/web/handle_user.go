package web

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-zoo/bone"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

type UpdateUserIconResponseV0 struct {
	User    domain.User `json:"user,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type UpdateUserBannerResponseV0 struct {
	User    domain.User `json:"user,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type UserInteractor interface {
	GetByUsername(username string) (domain.User, error)
	UpdateIcon(filePath string, user domain.User) (domain.User, error)
	UpdateBanner(filePath string, user domain.User) (domain.User, error)
}

func (handler *WebHandler) GetUserIcon(res http.ResponseWriter, req *http.Request) {
	username := bone.GetValue(req, "username")
	user, err := handler.UserInteractor.GetByUsername(username)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}
	res.Write(handler.util.ReadImage(user.Media.Icon.Source))
}

func (handler *WebHandler) GetUserBanner(res http.ResponseWriter, req *http.Request) {
	username := bone.GetValue(req, "username")
	user, err := handler.UserInteractor.GetByUsername(username)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}
	res.Write(handler.util.ReadImage(user.Media.Banner.Source))
}

func (handler *WebHandler) UpdateUserIcon(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	username := bone.GetValue(req, "username")
	user, err := handler.UserInteractor.GetByUsername(username)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if user.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Invalid identity"))
		return
	}

	file, fileDir, filePath, err := handler.util.DecodeImageFile(req, "icon")
	defer file.Close()
	defer os.RemoveAll(fileDir)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	user, err = handler.UserInteractor.UpdateIcon(filePath, user)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateUserIconResponseV0{
		User:    user,
		Message: "Icon updated",
		Success: true,
	})
}

func (handler *WebHandler) UpdateUserBanner(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	username := bone.GetValue(req, "username")
	user, err := handler.UserInteractor.GetByUsername(username)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if user.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Invalid identity"))
		return
	}

	file, fileDir, filePath, err := handler.util.DecodeImageFile(req, "banner")
	defer file.Close()
	defer os.RemoveAll(fileDir)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	user, err = handler.UserInteractor.UpdateBanner(filePath, user)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateUserBannerResponseV0{
		User:    user,
		Message: "Banner updated",
		Success: true,
	})
}
