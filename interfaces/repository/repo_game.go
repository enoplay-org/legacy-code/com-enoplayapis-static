package repository

import (
	"fmt"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

const GameCollection = "games"

type DbGameRepo DbRepo

func NewDbGameRepo(dbHandlers map[string]DbHandler) *DbGameRepo {
	dbGameRepo := &DbGameRepo{}
	dbGameRepo.dbHandlers = dbHandlers
	dbGameRepo.dbHandler = dbHandlers["DbGameRepo"]

	return dbGameRepo
}

func (repo *DbGameRepo) GetByUID(uid string) (domain.Game, error) {
	var game domain.Game
	err := repo.dbHandler.FindOne(GameCollection, Query{"uid": uid}, &game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error finding game by uid: %v", err)
	}
	return game, nil
}

func (repo *DbGameRepo) GetByGID(gid string) (domain.Game, error) {
	var game domain.Game
	err := repo.dbHandler.FindOne(GameCollection, Query{"gid": gid}, &game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error finding game by gid: %v", err)
	}
	return game, nil
}

func (repo *DbGameRepo) UpdateThumbnail(game domain.Game) (domain.Game, error) {
	update := Query{
		"media.thumbnail": game.Media.Thumbnail,
	}

	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &game)
	return game, err
}

func (repo *DbGameRepo) UpdateImages(game domain.Game) (domain.Game, error) {
	update := Query{
		"media.images": game.Media.Images,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &game)
	return game, err
}

func (repo *DbGameRepo) UpdateVideos(game domain.Game) (domain.Game, error) {
	update := Query{
		"media.videos": game.Media.Videos,
	}
	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	err := repo.dbHandler.Update(GameCollection, Query{"uid": game.UID}, change, &game)
	return game, err
}
