package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/enoplay/com-enoplayapis-static/infrastructure"
	"gitlab.com/enoplay/com-enoplayapis-static/interfaces/repository"
	"gitlab.com/enoplay/com-enoplayapis-static/interfaces/web"
	"gitlab.com/enoplay/com-enoplayapis-static/usecase"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-zoo/bone"
	"github.com/twinj/uuid"
	"github.com/urfave/negroni"
)

const (
	envPort                = "PORT"
	envFilePathUploadImage = "FILE_PATH_UPLOAD_IMAGE"
	envFileUploadMax       = "FILE_UPLOAD_MAX"
	envPrivateSigningKey   = "PRIVATE_SIGNING_KEY"
	envPublicSigningKey    = "PUBLIC_SIGNING_KEY"
	envMongoDatabase       = "MONGO_DATABASE"
	envMongoFullURL        = "MONGO_FULLURI"
	envCloudinaryURI       = "CLOUDINARY_URI"
)

func init() {
	// Initialize UUID generator
	uuid.Init()

	// Initialize global pseudo random generator
	rand.Seed(time.Now().UnixNano())

	// Initialize environment variables
	initEnvironmentVariables()
}

func main() {
	// Setup signing keys
	privateSigningKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(os.Getenv(envPrivateSigningKey)))
	if err != nil {
		log.Fatal("Error parsing private key")
		return
	}
	publicSigningKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(os.Getenv(envPublicSigningKey)))
	if err != nil {
		log.Fatal("Error parsing public key")
		return
	}

	// Setup MongoDB - Database service
	dbOptions := &infrastructure.MongoOptions{
		ServerName:   os.Getenv(envMongoFullURL),
		DatabaseName: os.Getenv(envMongoDatabase),
	}
	dbHandler := infrastructure.NewMongoHandler(dbOptions)
	dbHandler.NewSession()

	// Setup Cloudinary - Image processing + storage service
	cloudinaryOptions := infrastructure.CloudinaryOptions{
		URI: os.Getenv(envCloudinaryURI),
	}
	cloudinary := infrastructure.NewCloudinaryHandler(cloudinaryOptions)

	handlers := make(map[string]repository.DbHandler)
	handlers["DbSessionTokenRepo"] = dbHandler
	handlers["DbUserRepo"] = dbHandler
	handlers["DbGameRepo"] = dbHandler
	handlers["DbImageRepo"] = dbHandler

	sessionTokenInterator := usecase.NewSessionTokenInteractor()
	userInteractor := usecase.NewUserInteractor()
	gameInteractor := usecase.NewGameInteractor()
	imageInteractor := usecase.NewImageInteractor()

	tokenAuthorityOptions := repository.NewTokenAuthorityOptions(privateSigningKey, publicSigningKey)
	sessionTokenInterator.SessionTokenRepository = repository.NewDbSessionTokenRepo(handlers, tokenAuthorityOptions)
	userInteractor.UserRepository = repository.NewDbUserRepo(handlers)
	gameInteractor.GameRepository = repository.NewDbGameRepo(handlers)
	imageInteractor.ImageRepository = repository.NewDbImageRepo(handlers, cloudinary)

	userInteractor.ImageRepository = imageInteractor.ImageRepository
	gameInteractor.ImageRepository = imageInteractor.ImageRepository

	// Setup web handler
	maxFiles, err := strconv.Atoi(os.Getenv(envFileUploadMax))
	if err != nil {
		log.Fatalf("Error parsing max file upload environment variable: %v", err)
	}
	webHandlerOptions := web.WebHandlerOptions{
		FileUploadPathImage: os.Getenv(envFilePathUploadImage),
		MaxFiles:            maxFiles,
	}
	w := web.NewWebHandler(webHandlerOptions)
	w.SessionTokenInteractor = sessionTokenInterator
	w.ImageInteractor = imageInteractor
	w.UserInteractor = userInteractor
	w.GameInteractor = gameInteractor

	// Setup route handling
	router := bone.New()
	n := negroni.New()
	n.UseFunc(w.ForceHTTPS)
	n.UseFunc(w.CORS)
	n.UseFunc(w.MaxRequestSize)

	router.Get("/", http.HandlerFunc(w.GetIndex))
	v0 := bone.New()

	v0.Delete("/images/:uid", w.EnsureAuthorization(w.DeleteImage))

	v0.Get("/users/:username/icon", http.HandlerFunc(w.GetUserIcon))
	v0.Get("/users/:username/banner", http.HandlerFunc(w.GetUserBanner))
	v0.Post("/users/:username/icon", w.EnsureAuthorization(w.UpdateUserIcon))
	v0.Post("/users/:username/banner", w.EnsureAuthorization(w.UpdateUserBanner))

	v0.Get("/games/:gid/thumbnail", http.HandlerFunc(w.GetGameThumbnail))
	v0.Post("/games/:gid/thumbnail", w.EnsureAuthorization(w.UpdateGameThumbnail))
	v0.Post("/games/:gid/images", w.EnsureAuthorization(w.UpdateGameImages))
	v0.Post("/games/:gid/videos", w.EnsureAuthorization(w.UpdateGameVideos))

	// v0.Get("/.well-known/acme-challenge/ZRMOe-c_BflkHHu78gMOZoXQ_6U1nJVY0XGNvY5JrkQ", http.HandlerFunc(w.GetCertBotKey)) // Certbot - Certificate verification

	router.SubRoute("/v0", v0)
	n.UseHandler(router)
	fmt.Printf("Running on port: %v\n", os.Getenv(envPort))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", os.Getenv(envPort)), n))
}

// IndexConfig represents the expected structure of /config/index.json
type IndexConfig struct {
	Port                string `json:"port"`
	FilePathUploadImage string `json:"filePathUploadImage"`
	FileUploadMax       int    `json:"fileUploadMax"`
}

// MongoConfig represents the expected structure of /config/mongo.json
type MongoConfig struct {
	Database string `json:"database"`
	FullURI  string `json:"fullURI"`
}

// CloudinaryConfig represents the expected structure of /config/cloudinary.json
type CloudinaryConfig struct {
	URI string `json:"uri"`
}

func initEnvironmentVariables() {
	// Check if environment variables are provided (e.g. production)
	if len(os.Getenv(envPort)) != 0 {
		return
	}

	// Setup general settings
	indexFile, err := ioutil.ReadFile("config/index.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading index config file %v", err.Error()))
	}
	var indexConfig IndexConfig
	err = json.Unmarshal(indexFile, &indexConfig)

	// Setup PORT
	port := os.Getenv(envPort)
	if len(port) == 0 {
		os.Setenv(envPort, indexConfig.Port)
	}

	// Setup File Path Upload
	filePathUpload := os.Getenv(envFilePathUploadImage)
	if len(filePathUpload) == 0 {
		os.Setenv(envFilePathUploadImage, indexConfig.FilePathUploadImage)
	}

	fileUploadMax := os.Getenv(envFileUploadMax)
	if len(fileUploadMax) == 0 {
		os.Setenv(envFileUploadMax, strconv.Itoa(indexConfig.FileUploadMax))
	}

	// Setup Private Signing Key
	privateSigningKey := os.Getenv(envPrivateSigningKey)
	if len(privateSigningKey) == 0 {
		keyBytes, err2 := ioutil.ReadFile("config/keys/api.domain.key")
		if err2 != nil {
			panic(fmt.Errorf("Error loading private signing key: %v", err2.Error()))
		}
		os.Setenv(envPrivateSigningKey, string(keyBytes))
	}

	// Setup Public Signing Key
	publicSigningKey := os.Getenv(envPublicSigningKey)
	if len(publicSigningKey) == 0 {
		keyBytes, err2 := ioutil.ReadFile("config/keys/api.domain.key.pub")
		if err2 != nil {
			panic(fmt.Errorf("Error loading public signing key: %v", err2.Error()))
		}
		os.Setenv(envPublicSigningKey, string(keyBytes))
	}

	// Setup Mongo (Database and FullURI)
	mongoFile, err := ioutil.ReadFile("config/mongo.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading mailgun config file %v", err.Error()))
	}
	var mongoConfig MongoConfig
	err = json.Unmarshal(mongoFile, &mongoConfig)

	// Mongo Database
	mongoDatabase := os.Getenv(envMongoDatabase)
	if len(mongoDatabase) == 0 {
		os.Setenv(envMongoDatabase, mongoConfig.Database)
	}

	// Mongo FullURI
	mongoFullURI := os.Getenv(envMongoFullURL)
	if len(mongoFullURI) == 0 {
		os.Setenv(envMongoFullURL, mongoConfig.FullURI)
	}

	// Setup Cloudinary (URI)
	cloudinaryFile, err := ioutil.ReadFile("config/cloudinary.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Error loading cloudinary config file %v", err.Error()))
	}
	var cloudinaryConfig CloudinaryConfig
	err = json.Unmarshal(cloudinaryFile, &cloudinaryConfig)

	// Cloudinary URI
	cloudinaryURI := os.Getenv(envCloudinaryURI)
	if len(cloudinaryURI) == 0 {
		os.Setenv(envCloudinaryURI, cloudinaryConfig.URI)
	}
}
