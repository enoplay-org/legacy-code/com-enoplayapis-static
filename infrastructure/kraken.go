package infrastructure

import (
	"fmt"
	"log"

	kraken "github.com/kraken-io/kraken-go"
)

type KrakenOptions struct {
	ApiKey      string
	ApiSecret   string
	CallbackURL string
}

type KrakenHandler struct {
	k       *kraken.Kraken
	options KrakenOptions
}

func NewKrakenHandler(options KrakenOptions) *KrakenHandler {
	krakenHandler := &KrakenHandler{options: options}

	kr, err := kraken.New(krakenHandler.options.ApiKey, krakenHandler.options.ApiSecret)
	krakenHandler.k = kr
	if err != nil {
		log.Fatal(err)
	}

	return krakenHandler
}

func (kraken *KrakenHandler) Upload(imagePath string) (interface{}, error) {
	params := map[string]interface{}{
		"wait": true,
	}

	data, err := kraken.k.Upload(params, imagePath)
	if err != nil {
		return nil, err
	}

	fmt.Println(data)

	return data, nil
}
