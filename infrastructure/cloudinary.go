package infrastructure

import (
	"log"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"

	cloudinary "github.com/gotsunami/go-cloudinary"
)

type CloudinaryOptions struct {
	URI string
}

type CloudinaryHandler struct {
	service *cloudinary.Service
	options CloudinaryOptions
}

func NewCloudinaryHandler(options CloudinaryOptions) *CloudinaryHandler {
	cloudinaryHandler := &CloudinaryHandler{options: options}
	service, err := cloudinary.Dial(options.URI)
	if err != nil {
		log.Fatalf("Cloudinary service isn't available: %v", err.Error())
	}
	cloudinaryHandler.service = service
	return cloudinaryHandler
}

func (handler *CloudinaryHandler) Upload(imagePath string) (string, error) {
	publicID, err := handler.service.Upload(imagePath, nil, "", true, cloudinary.ImageType)
	if err != nil {
		return "", err
	}
	return publicID, nil
}

func (handler *CloudinaryHandler) Retrieve(imageID string) (domain.ImageDetails, error) {
	details, err := handler.service.ResourceDetails(imageID)
	if err != nil {
		return domain.ImageDetails{}, err
	}

	imageDetails := domain.ImageDetails{
		Source: details.SecureUrl,
	}
	return imageDetails, nil
}

func (handler *CloudinaryHandler) Remove(imageID string) error {
	err := handler.service.Delete(imageID, "", cloudinary.ImageType)
	return err
}
