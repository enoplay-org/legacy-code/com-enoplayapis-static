# 📸 [static.enoplayapis.com](https://static.enoplayapis.com)

> API for managing images for Enoplay.

## 📘 [Documentation](https://gitlab.com/enoplay-org/com-enoplayapis-static/tree/master/doc)

## 🔨 Build Setup

``` bash
# install dependencies
go get dependency-name

# update Godeps dependencies
godep save

# serve at localhost:8090
go run main.go

# run unit tests
go test
```
